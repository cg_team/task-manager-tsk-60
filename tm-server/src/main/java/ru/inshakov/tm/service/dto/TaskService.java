package ru.inshakov.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.inshakov.tm.api.repository.dto.ITaskRepository;
import ru.inshakov.tm.api.service.IConnectionService;
import ru.inshakov.tm.api.service.dto.ITaskService;
import ru.inshakov.tm.dto.Task;
import ru.inshakov.tm.enumerated.Status;
import ru.inshakov.tm.exception.empty.EmptyIdException;
import ru.inshakov.tm.exception.empty.EmptyIndexException;
import ru.inshakov.tm.exception.empty.EmptyNameException;
import ru.inshakov.tm.exception.entity.TaskNotFoundException;
import ru.inshakov.tm.repository.dto.TaskRepository;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class TaskService extends AbstractService<Task> implements ITaskService {

    @NotNull
    @Autowired
    private ITaskRepository repository;

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll() {
        return repository.findAll();
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable final Collection<Task> collection) {
        if (collection == null) return;
        for (Task item : collection) {
            add(item);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task add(@Nullable final Task entity) {
        if (entity == null) return null;
        repository.add(entity);
        return entity;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        return repository.findById(optionalId.orElseThrow(EmptyIdException::new));
    }

    @Override
    @SneakyThrows
    public void clear() {
        repository.clear();
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        repository.removeById(optionalId.orElseThrow(EmptyIdException::new));
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final Task entity) {
        if (entity == null) return;
        repository.removeById(entity.getId());
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        return repository.findByIndex(userId, index);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return repository.findByName(userId, name);
    }

    @Override
    @SneakyThrows
    public void removeByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        repository.removeByIndex(userId, index);
    }

    @Override
    @SneakyThrows
    public void removeByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        repository.removeByName(userId, name);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task updateById(
            @NotNull final String userId, @Nullable final String id,
            @Nullable final String name, @Nullable final String description) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        @NotNull final Task task = Optional.ofNullable(repository.findByIdUserId(userId, id))
                .orElseThrow(TaskNotFoundException::new);
        task.setName(name);
        task.setDescription(description);
        repository.update(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task updateByIndex(
            @NotNull final String userId, @Nullable final Integer index,
            @Nullable final String name, @Nullable final String description) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        @NotNull final Task task = Optional.ofNullable(repository.findByIndex(userId, index))
                .orElseThrow(TaskNotFoundException::new);
        task.setName(name);
        task.setDescription(description);
        repository.update(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task startById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final Task task = Optional.ofNullable(repository.findByIdUserId(userId, id))
                .orElseThrow(TaskNotFoundException::new);
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        repository.update(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task startByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final Task task = Optional.ofNullable(repository.findByIndex(userId, index))
                .orElseThrow(TaskNotFoundException::new);
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        repository.update(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task startByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Task task = Optional.ofNullable(repository.findByName(userId, name))
                .orElseThrow(TaskNotFoundException::new);
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        repository.update(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task finishById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final Task task = Optional.ofNullable(repository.findByIdUserId(userId, id))
                .orElseThrow(TaskNotFoundException::new);
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        repository.update(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task finishByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final Task task = Optional.ofNullable(repository.findByIndex(userId, index))
                .orElseThrow(TaskNotFoundException::new);
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        repository.update(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task finishByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Task task = Optional.ofNullable(repository.findByName(userId, name))
                .orElseThrow(TaskNotFoundException::new);
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        repository.update(task);
        return task;
    }

    @SneakyThrows
    @Nullable
    public Task add(String user, @Nullable String name, @Nullable String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final Task task = new Task(name, description);
        add(user, task);
        return (task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll(@NotNull final String userId) {
        return repository.findAllByUserId(userId);
    }

    @Override
    @SneakyThrows
    public void addAll(final String userId, @Nullable final Collection<Task> collection) {
        if (collection == null || collection.isEmpty()) return;
        addAll(collection);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task add(final String user, @Nullable final Task entity) {
        if (entity == null) return null;
        entity.setUserId(user);
        @Nullable final Task entityResult = add(entity);
        return entityResult;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        return repository.findByIdUserId(userId, optionalId.orElseThrow(EmptyIdException::new));
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        repository.clearByUserId(userId);
    }

    @Override
    @SneakyThrows
    public void removeById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        repository.removeByIdUserId(userId, optionalId.orElseThrow(EmptyIdException::new));
    }

    @Override
    @SneakyThrows
    public void remove(@NotNull final String userId, @Nullable final Task entity) {
        if (entity == null) return;
        repository.removeByIdUserId(userId, entity.getId());
    }

}
