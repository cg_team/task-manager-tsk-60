package ru.inshakov.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.inshakov.tm.api.repository.model.IProjectGraphRepository;
import ru.inshakov.tm.api.repository.model.ITaskGraphRepository;
import ru.inshakov.tm.api.service.IConnectionService;
import ru.inshakov.tm.api.service.dto.IProjectTaskService;
import ru.inshakov.tm.api.service.dto.ITaskService;
import ru.inshakov.tm.api.service.model.IProjectGraphService;
import ru.inshakov.tm.api.service.model.IProjectTaskGraphService;
import ru.inshakov.tm.api.service.model.ITaskGraphService;
import ru.inshakov.tm.exception.empty.EmptyIdException;
import ru.inshakov.tm.exception.empty.EmptyIndexException;
import ru.inshakov.tm.exception.empty.EmptyNameException;
import ru.inshakov.tm.exception.entity.ProjectNotFoundException;
import ru.inshakov.tm.model.TaskGraph;
import ru.inshakov.tm.repository.model.ProjectGraphRepository;
import ru.inshakov.tm.repository.model.TaskGraphRepository;
import ru.inshakov.tm.service.AbstractEntityService;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ProjectTaskGraphService extends AbstractEntityService implements IProjectTaskGraphService {

    @NotNull
    @Autowired
    private ITaskGraphRepository repository;

    @NotNull
    @Autowired
    private IProjectGraphRepository projectRepository;

    @Nullable
    @Override
    @SneakyThrows
    public List<TaskGraph> findTaskByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException("Project");
        return repository.findAllTaskByProjectId(userId, projectId);
    }

    @Override
    @SneakyThrows
    public void bindTaskById(
            @NotNull final String userId, @Nullable final String taskId, @Nullable final String projectId) {
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException("Task");
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException("Project");
        repository.bindTaskToProjectById(userId, taskId, projectId);
    }

    @Override
    @SneakyThrows
    public void unbindTaskById(@NotNull final String userId, @Nullable final String taskId) {
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException("Task");
        repository.unbindTaskById(userId, taskId);
    }

    @Override
    @SneakyThrows
    public void removeProjectById(@NotNull final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException("Project");
        repository.removeAllTaskByProjectId(userId, projectId);
        projectRepository.removeByIdUserId(userId, projectId);
    }


    @Override
    @SneakyThrows
    public void removeProjectByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final String projectId = Optional.ofNullable(projectRepository.findByIndex(userId, index))
                .orElseThrow(ProjectNotFoundException::new)
                .getId();
        repository.removeAllTaskByProjectId(userId, projectId);
        projectRepository.removeByIdUserId(userId, projectId);
    }

    @Override
    @SneakyThrows
    public void removeProjectByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final String projectId = Optional.ofNullable(projectRepository.findByName(userId, name))
                .orElseThrow(ProjectNotFoundException::new)
                .getId();
        repository.removeAllTaskByProjectId(userId, projectId);
        projectRepository.removeByIdUserId(userId, projectId);
    }

}