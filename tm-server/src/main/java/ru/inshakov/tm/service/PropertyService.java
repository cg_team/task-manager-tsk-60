package ru.inshakov.tm.service;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.inshakov.tm.api.IPropertyService;
import ru.inshakov.tm.api.other.ISignatureSetting;

import java.io.InputStream;
import java.util.Properties;

import static ru.inshakov.tm.constant.PropertyConst.*;

@Getter
@Service
@PropertySource("classpath:application.properties")
public final class PropertyService implements IPropertyService, ISignatureSetting {

    @Value("#{environment['secret']}")
    public String passwordSecret;

    @Value("#{environment['iteration']}")
    public Integer passwordIteration;

    @Value("#{environment['version']}")
    public String applicationVersion;

    @Value("#{environment['host']}")
    public String serverHost;

    @Value("#{environment['port']}")
    public String serverPort;

    @Value("#{environment['sign.secret']}")
    public String signatureSecret;

    @Value("#{environment['sign.iteration']}")
    public Integer signatureIteration;

    @Value("#{environment['jdbc.user']}")
    public String jdbcUser;

    @Value("#{environment['jdbc.password']}")
    public String jdbcPassword;

    @Value("#{environment['jdbc.url']}")
    public String jdbcUrl;

    @Value("#{environment['jdbc.driver']}")
    public String jdbcDriver;

    @Value("#{environment['hibernate.dialect']}")
    public String hibernateDialect;

    @Value("#{environment['hibernate.hbm2ddl_auto']}")
    public String hibernateBM2DDLAuto;

    @Value("#{environment['hibernate.show_sql']}")
    public String hibernateShowSql;

    @Value("#{environment['hibernate.cache.use_second_level_cache']}")
    public String secondLevelCash;

    @Value("#{environment['hibernate.cache.use_query_cache']}")
    public String queryCache;

    @Value("#{environment['hibernate.cache.use_minimal_puts']}")
    public String minimalPuts;

    @Value("#{environment['hibernate.cache.hazelcast.use_lite_member']}")
    public String liteMember;

    @Value("#{environment['hibernate.cache.region_prefix']}")
    public String regionPrefix;

    @Value("#{environment['hibernate.cache.provider_configuration_file_resource_path']}")
    public String cacheProvider;

    @Value("#{environment['hibernate.cache.region.factory_class']}")
    public String factoryClass;

}
