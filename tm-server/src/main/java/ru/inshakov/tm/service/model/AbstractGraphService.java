package ru.inshakov.tm.service.model;

import com.google.common.graph.AbstractGraph;
import org.jetbrains.annotations.NotNull;
import ru.inshakov.tm.api.service.IConnectionService;
import ru.inshakov.tm.model.AbstractGraphEntity;
import ru.inshakov.tm.service.AbstractEntityService;
import ru.inshakov.tm.service.dto.AbstractService;

public abstract class AbstractGraphService<E extends AbstractGraphEntity> extends AbstractEntityService {

}
