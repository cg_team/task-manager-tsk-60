package ru.inshakov.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.inshakov.tm.api.repository.dto.IProjectRepository;
import ru.inshakov.tm.api.repository.dto.ITaskRepository;
import ru.inshakov.tm.api.service.dto.IProjectTaskService;
import ru.inshakov.tm.dto.Task;
import ru.inshakov.tm.exception.empty.EmptyIdException;
import ru.inshakov.tm.exception.empty.EmptyIndexException;
import ru.inshakov.tm.exception.empty.EmptyNameException;
import ru.inshakov.tm.exception.entity.ProjectNotFoundException;
import ru.inshakov.tm.repository.dto.ProjectRepository;
import ru.inshakov.tm.repository.dto.TaskRepository;
import ru.inshakov.tm.service.AbstractEntityService;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ProjectTaskService extends AbstractEntityService implements IProjectTaskService {

    @NotNull
    @Autowired
    private ITaskRepository repository;

    @NotNull
    @Autowired
    private IProjectRepository projectRepository;

    @Nullable
    @Override
    @SneakyThrows
    public List<Task> findTaskByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException("Project");
        return repository.findAllTaskByProjectId(userId, projectId);
    }

    @Override
    @SneakyThrows
    public void bindTaskById(
            @NotNull final String userId, @Nullable final String taskId, @Nullable final String projectId) {
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException("Task");
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException("Project");
        repository.bindTaskToProjectById(userId, taskId, projectId);
    }

    @Override
    @SneakyThrows
    public void unbindTaskById(@NotNull final String userId, @Nullable final String taskId) {
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException("Task");
        repository.unbindTaskById(userId, taskId);
    }

    @Override
    @SneakyThrows
    public void removeProjectById(@NotNull final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException("Project");
        repository.removeAllTaskByProjectId(userId, projectId);
        projectRepository.removeByIdUserId(userId, projectId);
    }


    @Override
    @SneakyThrows
    public void removeProjectByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final String projectId = Optional.ofNullable(projectRepository.findByIndex(userId, index))
                .orElseThrow(ProjectNotFoundException::new)
                .getId();
        repository.removeAllTaskByProjectId(userId, projectId);
        projectRepository.removeByIdUserId(userId, projectId);
    }

    @Override
    @SneakyThrows
    public void removeProjectByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final String projectId = Optional.ofNullable(projectRepository.findByName(userId, name))
                .orElseThrow(ProjectNotFoundException::new)
                .getId();
        repository.removeAllTaskByProjectId(userId, projectId);
        projectRepository.removeByIdUserId(userId, projectId);
    }

}