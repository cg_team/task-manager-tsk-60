package ru.inshakov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.activemq.broker.BrokerService;
import org.apache.log4j.BasicConfigurator;
import org.springframework.beans.factory.annotation.Autowired;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import ru.inshakov.tm.api.IPropertyService;
import ru.inshakov.tm.api.service.*;
import ru.inshakov.tm.api.service.dto.IProjectService;
import ru.inshakov.tm.api.service.dto.IProjectTaskService;
import ru.inshakov.tm.api.service.dto.ISessionService;
import ru.inshakov.tm.api.service.dto.ITaskService;
import ru.inshakov.tm.api.service.dto.IUserService;
import ru.inshakov.tm.api.service.model.IProjectGraphService;
import ru.inshakov.tm.api.service.model.ISessionGraphService;
import ru.inshakov.tm.api.service.model.ITaskGraphService;
import ru.inshakov.tm.api.service.model.IUserGraphService;
import ru.inshakov.tm.component.MessageExecutor;
import ru.inshakov.tm.dto.Project;
import ru.inshakov.tm.dto.Task;
import ru.inshakov.tm.dto.User;
import ru.inshakov.tm.endpoint.*;
import ru.inshakov.tm.enumerated.Status;
import ru.inshakov.tm.service.*;
import ru.inshakov.tm.service.dto.*;
import ru.inshakov.tm.service.model.ProjectGraphService;
import ru.inshakov.tm.service.model.SessionGraphService;
import ru.inshakov.tm.service.model.TaskGraphService;
import ru.inshakov.tm.service.model.UserGraphService;
import ru.inshakov.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Objects;
import java.util.Set;

@Getter
@Component
public class Bootstrap {

    @NotNull
    @Autowired
    protected ApplicationContext context;

    @NotNull
    @Autowired
    private static MessageExecutor messageExecutor;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Autowired
    private Backup backup;

    @Nullable
    @Autowired
    private AbstractEndpoint[] endpoints;


    public void init() {
        initEndpoints();
    }

    @SneakyThrows
    public void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    @SneakyThrows
    public void initJMSBroker() {
        BasicConfigurator.configure();
        @NotNull final BrokerService brokerService = new BrokerService();
        brokerService.addConnector("tcp://localhost:61616");
        brokerService.start();
    }

    private void registry(final Object endpoint) {
        if (endpoint == null) return;
        final String host = propertyService.getServerHost();
        final String port = propertyService.getServerPort();
        final String name = endpoint.getClass().getSimpleName();
        final String wsdl = "http://" + host + ":" + port + "/" + name + "?WSDL";
        System.out.println(wsdl);
        Endpoint.publish(wsdl,endpoint);
    }

    @SneakyThrows
    private void initEndpoints() {
        if (endpoints == null) return;
        Arrays.stream(endpoints).filter(Objects::nonNull).forEach(this::registry);
    }

    public static void sendMessage(@Nullable final Object record,
                                   @NotNull final String type) {
        messageExecutor.sendMessage(record, type);
    }

}
