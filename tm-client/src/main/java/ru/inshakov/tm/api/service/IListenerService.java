package ru.inshakov.tm.api.service;

import ru.inshakov.tm.listener.AbstractListener;

import java.util.Collection;

public interface IListenerService {

    AbstractListener getListenerByName(String name);

    AbstractListener getListenerByArg(String arg);

    Collection<AbstractListener> getListeners();

    Collection<AbstractListener> getArguments();

    Collection<String> getListListenerName();

    Collection<String> getListListenerArg();

    void add(AbstractListener listener);
}
